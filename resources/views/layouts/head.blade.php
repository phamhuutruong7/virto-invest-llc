<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="A fat free flat responsive corporate agency templae">
    <meta name="keywords" content="Agency, Corporate, Flat, Responsive">

    <title>R E ! G N - Home Multi-Page | Fat free responsive agency template</title>

    <link rel="shortcut icon" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/favicon.ico') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-TileImage" content="{{ asset('templates/Reign-v2.1/template/assets/images/favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#000000">


    <!-- css -->
    <link rel="stylesheet" href="{{ asset('templates/Reign-v2.1/template/assets/lib/bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/Reign-v2.1/template/assets/lib/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/Reign-v2.1/template/assets/lib/ionicons/css/ionicons.cs') }}s">
    <link rel="stylesheet" href="{{ asset('templates/Reign-v2.1/template/assets/lib/owlcarousel/owl-carousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/Reign-v2.1/template/assets/lib/owlcarousel/owl-carousel/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/Reign-v2.1/template/assets/lib/FlexSlider/flexslider.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/Reign-v2.1/template/assets/lib/magnific-popup/dist/magnific-popup.cs') }}s"/>

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Raleway:100,300,400">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:100,300,400">

    <link rel="stylesheet" href="{{ asset('templates/Reign-v2.1/template/assets/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/Reign-v2.1/template/assets/css/ie_fix.css') }}">

    <script src="{{ asset('templates/Reign-v2.1/template/assets/lib/components-modernizr/modernizr.js') }}"></script>
    <script src="{{ asset('templates/Reign-v2.1/template/assets/lib/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('templates/Reign-v2.1/template/assets/lib/bootstrap/dist/js/bootstrap.js') }}"></script>

</head>