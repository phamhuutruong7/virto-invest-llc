<!-- footer -->
<footer id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <p class="copyright text-xs-center">&copy; 2019 This website is intellectual property of the Virto Invest LLC. and falls under copyright.</p>
            </div>

          {{--  <div class="col-md-5 col-sm-4">
                <ul class="footer-social-block">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>

                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>

                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>

                    <li><a href="#"><i class="fa fa-behance"></i></a></li>

                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>

                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>

                </ul>
            </div>--}}

            <div class="col-md-4 col-sm-4">
                <div class="footer-menu text-xs-center">
                    <a href="#">Privacy Policy</a> | <a href="">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- #footer -->

<a id="totop" href="#totop"><i class="fa fa-angle-double-up"></i></a>
