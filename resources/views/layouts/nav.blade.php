<!-- Fixed Top Navigation -->
<nav id="fixedTopNav" class="navbar navbar-fixed-top main-navigation" itemscope itemtype="https://schema.org/SiteNavigationElement" >
    <div class="container">
{{--
        <div class="navbar-header" style="width:25%; height: 25%;">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="ion-drag"></span>
            </button>

            <!-- Logo -->
            <div class="navbar-brand" itemscope itemtype="https://schema.org/Organization">
                <span itemprop="name" class="sr-only">Reign</span>
                <a itemprop="url" href="index.html")>
                    <img src="{{ asset('storage/images/virtologo.png') }}" >
                </a>
            </div>
            <!-- /Logo -->

        </div><!-- /.navbar-header -->--}}
        <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="ion-drag"></span>
            </button>

            <!-- Logo -->
            <div class="navbar-brand mt-3" itemscope="" itemtype="https://schema.org/Organization" style="width:200px;">
                <span itemprop="name" class="sr-only">Reign</span>
                <img src="{{ asset('storage/images/virtologo.png') }}" >
            </div>
            <!-- /Logo -->

        </div>

        <!-- Navigation Links -->
        <div class="collapse navbar-collapse" id="main-nav-collapse">
            <ul class="nav navbar-nav navbar-right" role="menu">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>Home</span></a>
                    <ul class="dropdown-menu">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">One Page</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__image_full_screen.html')}}">Image Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__image_classic.html')}}">Image Classic</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__slider_full_screen.html')}}">Slider Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__slider_classic.html')}}">Slider Classic</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__text_rotator_full_screen.html')}}">Text Rotator Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__text_rotator_classic.html')}}">Text Rotator Classic</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__gradient_overlay_full_screen.html')}}">Gradient Overlay Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__gradient_overlay_classic.html')}}">Gradient Overlay Classic</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__video_background_full_screen.html')}}">Video Background Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__op__video_background_classic.html')}}">Video Background Classic</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Multi Page</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('templates/Reign-v2.1/template/index.html')}}">Image Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__mp__image_classic.html')}}">Image Classic</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__mp__slider_full_screen.html')}}">Slider Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__mp__slider_classic.html')}}">Slider Classic</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__mp__text_rotator_full_screen.html')}}">Text Rotator Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__mp__text_rotator_classic.html')}}">Text Rotator Classic</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__mp__gradient_overlay_full_screen.html')}}">Gradient Overlay Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__mp__gradient_overlay_classic.html')}}">Gradient Overlay Classic</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__mp__video_background_full_screen.html')}}">Video Background Full Screen</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/index__mp__video_background_classic.html')}}">Video Background Classic</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>Pages</span></a>
                    <ul class="dropdown-menu">

                        <li>
                            <a href="{{asset('templates/Reign-v2.1/template/about_us.html')}}">About</a>
                        </li>

                        <li>
                            <a href="{{asset('templates/Reign-v2.1/template/services.html')}}">Services</a>
                        </li>

                        <li>
                            <a href="{{asset('templates/Reign-v2.1/template/pricing.html')}}">Pricing</a>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gallery</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('templates/Reign-v2.1/template/gallery__3_columns.html')}}">3 Columns</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/gallery__4_columns.html')}}">4 Columns</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/gallery__5_columns.html')}}">5 Columns</a></li>
                            </ul>
                        </li>

                        <li><a href="{{asset('templates/Reign-v2.1/template/faq.html')}}">FAQ</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('templates/Reign-v2.1/template/contact__classic_map.html')}}">Classic Style</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/contact__full_width_map.html')}}">Full Width Map</a></li>
                            </ul>
                        </li>
                        <li><a href="{{asset('templates/Reign-v2.1/template/404.html')}}">404 Page</a></li>

                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Portfolio</a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{asset('templates/Reign-v2.1/template/portfolio__boxed__lightbox.html')}}">Portfolio Lightbox</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Boxed</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('templates/Reign-v2.1/template/portfolio__boxed__2_columns.html')}}">2 Columns</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/portfolio__boxed__3_columns.html')}}">3 Columns</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Wide</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('templates/Reign-v2.1/template/portfolio__wide__2_columns.html')}}">2 Columns</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/portfolio__wide__3_columns.html')}}">3 Columns</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/portfolio__wide__4_columns.html')}}">4 Columns</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="{{asset('templates/Reign-v2.1/template/portfolio__single.html')}}">Single Portfolio</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>Blog</span></a>
                    <ul class="dropdown-menu">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Standard</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('templates/Reign-v2.1/template/blog__standard__left_sidebar.html')}}">Left Sidebar</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/blog__standard__right_sidebar.html')}}">Right Sidebar</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/blog__standard__full_width.html')}}">Full Width</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Grid</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('templates/Reign-v2.1/template/blog__grid__2_columns.html')}}">2 Columns</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/blog__grid__3_columns.html')}}">3 Columns</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/blog__grid__with_sidebar.html')}}">With Sidebar</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Single Blog Post</a>
                            <ul class="dropdown-menu">
                                <li><a href="{{asset('templates/Reign-v2.1/template/blog__single__left_sidebar.html')}}">Left Sidebar</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/blog__single__right_sidebar.html')}}">Right Sidebar</a></li>
                                <li><a href="{{asset('templates/Reign-v2.1/template/blog__single__full_width.html')}}">Full Width</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span>Components</span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{asset('templates/Reign-v2.1/template/icons.html')}}"><i class="fa fa-heart"></i> Icons</a></li>
                        <li><a href="{{asset('templates/Reign-v2.1/template/typography.html')}}"><i class="fa fa-font"></i> Typography</a></li>
                        <li><a href="{{asset('templates/Reign-v2.1/template/buttons.html')}}"><i class="fa fa-plus-square"></i> Buttons</a></li>
                        <li><a href="{{asset('templates/Reign-v2.1/template/forms.html')}}"><i class="fa fa-check-square"></i> Forms</a></li>
                        <li><a href="{{asset('templates/Reign-v2.1/template/tabs_and_accordions.html')}}"><i class="fa fa-indent"></i> Tabs &amp; Accordions</a></li>
                        <li><a href="{{asset('templates/Reign-v2.1/template/alerts_and_wells.html')}}"><i class="fa fa-bolt"></i> Alerts &amp; Wells</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- /Navigation Links -->

    </div><!-- /.container -->
</nav>
<!-- /Fixed Top Navigation -->